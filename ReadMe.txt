* Is there anything that doesn't work? Why?
        Nope. Should be fully functional. The push to git may be a bit odd, but the source code is there. One thing that does bug me that I wish I had more time to implement
        is part of the creative portion. When in the playlist, and you click on the track, it launches the TrackInfoActivity. From here, you should be able to click the title or
        artist name and it will return to the main activity and disply the results of the query in the home fragment. However, while this is functional, it returns to the tab view 
        showing the playlist and once you swipe left you see the results. I would have liked for it to return immediately to the home fragment with the results.


* Is there anything that you did that you feel might be unclear? Explain it here.
        The settings menu at the top right, when clicked attributes the API at last.fm as the source of data for the app. To delete a song from the playlist, you must long click on it.
        

A description of the creative portion of the assignment
* Describe your feature
        For the creative portion of the app, I made the artist name and the track title clickable in the TrackInfoActivity. When clicked the app returns to the MainActivity and displays
        similar artists or tracks accordingly.
        
        Some other small things: gradient background, rounded corners on images
        
        
        
* Why did you choose this feature?
        I chose this feature because when you lkike a song or an artist, you are likely to like similar songs and artists. It also reminded me of netflix/spotify recommendation systems.


* How did you implement it?
        The implementation was straight forward for the most part. Making the textViews clickable, implementing two new queries and async tasks were all simple. Furthermore, the fragments
        were nearly identical, and I was able to reuse the same Track and Artist models. The difficult part was passing the information back from the TrackInfoActivity to the MainActivity.
        In order to do so, I used a startActivityForResult intent and used an onActivityResult callback to retrieve the info. I sent the intents back to the MainActivity with extras. When
        an artist name or track is clicked, they both send back an instance of the track model, however they carry different keys. The mainActivity reacts according to these keys. Furthermroe,
        I was getting a state loss error when trying to display a new fragment that was not the last fragment that was being displayed in the MainActivity when it was last paused. In order to
        circumvent this error, I had to tell my fragmentSupportManager to commitAllowingStateLoss().
        
(10 / 10 points) The app displays the current top tracks in a GridView on startup
(10 / 10 points) The app uses a tab bar with two tabs, one for searching for tracks and one for looking at the playlist
(10 / 10 points) Data is pulled from the API and processed into a GridView on the main page. Makes use of a Fragment to display the results seamlessly.
(15 / 15 points) Selecting a track from the GridView opens a new activity with the track cover, title, and 3 other pieces of information as well as the ability to save it to the playlist.
(5 / 5 points) User can change search query by editing text field.
(10 / 10 points) User can save a track to their playlist, and the track is saved into a SQLite database.
(5 / 5 points) User can delete a track from the playlist (deleting it from the SQLite database itself).
(4 / 4 points) App is visually appealing
(1/ 1 point) Properly attribute Last.fm API as source of data.
(5 / 5 points) Code is well formatted and commented.
(10 / 10 points) All API calls are done asynchronously and do not stall the application.
(15 / 15 points) Creative portion: Be creative!

Total: 100 / 100

Nice creative features. Your app also looks fantastic. Great job!